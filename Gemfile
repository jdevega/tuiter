source 'https://rubygems.org'

ruby '2.2.4'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# jQuery plugin for drop-in fix binded events problem caused by Turbolinks
gem 'jquery-turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  # Small gem which causes rails console to open pry
  gem 'pry-rails'
  # Better error page for Rack apps
  gem 'better_errors'
  # Retrieve the binding of a method's caller in MRI 1.9.2+
  gem 'binding_of_caller'
  # Mutes assets pipeline log messages.
  gem 'quiet_assets'
  # Server deploy
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-rvm'
  # gem 'rack-mini-profiler'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.1'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'faker'
  gem 'database_cleaner', '~> 1.3.0'
  gem 'capybara', '~> 2.6'
end

gem 'unicorn'
gem 'devise'
gem 'omniauth'
gem 'omniauth-twitter'
gem 'omniauth-facebook'

gem 'friendly_id', '~> 5.1.0'
gem 'rest_in_place'

