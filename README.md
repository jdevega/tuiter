# TUITER #

A Twitter clone.

### Requirements ###

* ruby 2.2.4
* rails 4.2.6

### How do I get set up? ###

* Get the code
    * First of all, clone this repo :D
    * cd /tuiter
    * bundle 
* Configuration
    * Change the app domain to match your dev environment ( config.app_domain = 'dev.tuiter.com' )
    * Change the IP to allow better_errors ( BetterErrors::Middleware.allow_ip! '192.168.33.19' )
* Database configuration
    * The app uses SQlite3, so no need to config
* How to run tests
    * rake
* How to seed the database
    * rake db:seed
* How to start the app
    * Create the database and seed with "rake db:reset"
    * Start the server with "rails s"