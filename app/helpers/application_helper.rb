module ApplicationHelper
  
  AVATAR_SIZES = {
    thumb:  24,
    normal: 48,
    big:    96
  }

  def avatar_url(user, **options)
    size        = AVATAR_SIZES[options.fetch(:size, :thumb)]
    tooltip     = options.fetch(:tooltip, true) ? { toggle: "tooltip", placement: "top", title: user.username } : {};
    gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
    
    image_tag "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=identicon", class: 'profile-avatar', data: tooltip
  end

  def follow_button(user, **options)
    return if user == current_user
    remote = options.fetch(:remote, true)
    if current_user.follow? user
      link_to 'Unfollow', unfollow_user_url(user), 'data-remote': remote, class: 'btn btn-danger btn-unfollow', method: 'DELETE' 
    else
      link_to 'Follow', follow_user_url(user), 'data-remote': remote, class: 'btn btn-primary btn-follow', method: 'POST'
    end
  end

  def cache_key_for_relation(name, items)
    [current_user.id, name, items.count, (items.maximum(:updated_at).try(:utc).try(:to_s, :number) rescue 'empty')].join('/')
  end
end
