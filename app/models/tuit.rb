class Tuit < ActiveRecord::Base
  belongs_to :user

  default_scope { order(created_at: :desc) }

  validates :user_id, presence: true
  validates :text, presence: true, length: { maximum: 160 }
end
