class Relation < ActiveRecord::Base
  belongs_to :user
  belongs_to :follower, class_name: 'User', foreign_key: 'follow_id'

  validates :follower, uniqueness: { scope: :user}
end
