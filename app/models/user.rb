class User < ActiveRecord::Base

  extend FriendlyId
  friendly_id :username

  attr_accessor :login

  EMAIL_REGEX = /\A([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  USERNAME_REGEX =  /\A[\w-]+\z/


  # Include default devise modules. Others available are:
  # :lockable, :timeoutable
  devise :database_authenticatable, :registerable, :confirmable,
    :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  
  validates_format_of :email, :with => EMAIL_REGEX
  validates_format_of :username, :with => USERNAME_REGEX
  validates :username, presence: true, uniqueness: { case_sensitive: false }

  has_many :tuits
  has_many :following_to, foreign_key: 'user_id', class_name: 'Relation'
  has_many :followed_by, foreign_key: 'follow_id', class_name: 'Relation'
  has_many :followings, through: :following_to, source: :follower
  has_many :followers, through: :followed_by, source: :user
  has_many :followings_tuits, through: :followings, source: :tuits

  scope :all_except, ->(user) { where.not(id: user) }

  def follow?(user)
    get_followings.include? user
  end

  def timeline_tuits
    Tuit.where(user: followings.pluck(:id) << id)
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      conditions[:email].downcase! if conditions[:email]
      where(conditions.to_hash).first
    end
  end

  def self.find_for_oauth(auth, signed_in_resource = nil)

    # Get the identity and user if they exist
    identity = Identity.find_for_oauth(auth)

    # If a signed_in_resource is provided it always overrides the existing user
    # to prevent the identity being locked with accidentally created accounts.
    # Note that this may leave zombie accounts (with no associated identity) which
    # can be cleaned up at a later date.
    user = signed_in_resource ? signed_in_resource : identity.user

    # Create the user if needed
    if user.nil?

      # Get the existing user by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # user to verify it on the next step via UsersController.finish_signup
      # email_is_verified = auth.info.email && (auth.info.verified || auth.info.verified_email)
      email = auth.info.email
      user = User.where(:email => email).first if email

      # Create the user if it's a new registration
      if user.nil?
        user = User.new(
          name: auth.extra.raw_info.name,
          username: auth.info.nickname || auth.uid,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )
        user.skip_confirmation!
        user.save!
      end
    end

    # Associate the identity with the user if needed
    if identity.user != user
      identity.user = user
      identity.save!
    end
    user
  end

  private 

  def get_followings
    # Avoid N+1 query for followings.include?(user)
    @followings ||= self.followings.to_a
  end
end