class TuitsController < ApplicationController
  before_action :authenticate_user!
  before_action :build_tuit_from_params, only: [:create]
  
  # POST /tuits
  def create
    if @tuit.save
      redirect_to root_url
    else
      render 'home/timeline'
    end
  end

  private

  def tuits_params
    params.require(:tuit).permit([:text, :user_id])
  end

  def build_tuit_from_params
    @tuit = Tuit.new tuits_params.merge({ user_id: current_user.id })
    return true
  end
end