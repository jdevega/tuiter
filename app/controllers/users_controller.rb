class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, except: [:index]

  # GET /users
  def index
    @followings = current_user.followings
    @users      = User.all_except(current_user)
  end
  # GET /users/:id.:format
  def show
    @following        = @user.followings
    @followers        = @user.followers
    @followers_count  = @followers.count
    @following_count  = @following.count
    @tuits            = @user.tuits
  end

  # PATCH/PUT /users/:id.:format
  def update
    respond_to do |format|
      if @user.update(user_params)
        sign_in(@user == current_user ? @user : current_user, :bypass => true)
        format.html { redirect_to @user, notice: 'Your profile was successfully updated.' }
      else
        format.html { render action: 'edit' }
        format.json { render nothing: true }
      end
    end
  end

  # GET /users/:id/following(.json)
  def following
    render json: @user.followings
  end

  # GET /users/:id/followers(.json)
  def followers
    render json: @user.followers
  end

  # GET /users/:id/tuits(.json)
  def tuits
    render json: @user.tuits 
  end

  # POST /users/:id/follow.:format 
  def follow
    build_relation_from_params
    if @relation.save
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    else
      respond_to do |format|
        format.html { redirect_to root_url, error: 'Something bad happened, call SOMEONE ! QUICK !!' }
        format.js
      end
    end
  end

  # DELETE /users/:id/unfollow.:format 
  def unfollow
    build_relation_from_params
    if @relation.destroy
      respond_to do |format|
        format.html { redirect_to :back }
        format.js
      end
    else
      respond_to do |format|
        format.html { redirect_to root_url, error: 'Something bad happened, call SOMEONE ! QUICK !!' }
        format.js
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    accessible = [ :name, :email, :username ] # extend with your own params
    accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
    params.require(:user).permit(accessible)
  end

  def build_relation_from_params
    @relation = Relation.find_or_create_by(user_id: current_user.id, follow_id: @user.id)
  end
end