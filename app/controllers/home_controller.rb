class HomeController < ApplicationController
  before_action :authenticate_user!
  before_action :user_tuits, on: [:index]

  def index
    render :timeline if user_signed_in?
  end
 
  # No data to pass to the view, so there's no need to define the method
  # def contact
  # end

  private

  def user_tuits
    @tuits = user_signed_in? ? current_user.timeline_tuits : []
  end
  
end