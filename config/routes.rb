Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  root 'home#index'
  get '/contact', to: 'home#contact'
  resources :users, only: [:index, :show, :update] do 
    member do
      scope constraints: { format: /(html|js)/ } do
        post 'follow'
        delete 'unfollow'
      end
      scope constraints: { format: :json } do
        get 'following'
        get 'followers'
        get 'tuits'
      end
    end
  end
  resources :tuits, only: [:create]
end
