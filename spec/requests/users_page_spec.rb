require 'rails_helper'

RSpec.describe "users page", type: :request do

  let(:user1) { build(:user) }
  let(:user2) { build(:user) }
  let(:user_box_selector) { "[data-user-id='#{user2.username}']" }

  before(:each) do
    user1.skip_confirmation!
    user1.save!
    user2.skip_confirmation!
    user2.save!

    login(user1)
    visit '/users'
  end

  it 'show unfollow link after following an user' do
    within(user_box_selector) do
      click_link 'Follow'
    end

    within(user_box_selector) do
      expect(page).to have_link 'Unfollow'
    end
  end

  it 'show follow link after unfollow an user' do
    user1.followings << user2
    visit '/users'

    within(user_box_selector) do
      click_link 'Unfollow'
    end

    within(user_box_selector) do
      expect(page).to have_link 'Follow'
    end
  end

end
