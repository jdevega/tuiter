require 'rails_helper'

RSpec.describe "home page", type: :request do

  let(:user) { build(:user) }
  before(:each) do
    user.skip_confirmation!
    user.save!
  end

  it 'display user menu after successful login' do
    login(user)

    expect(page).to have_selector(".navbar-nav li.dropdown .name", text: user.name)
  end

  it 'display tuit in my timeline after tuit' do
    text = 'hola que tal'
    
    login(user)
    visit '/'

    fill_in 'tuit_text', with: text
    click_button 'Tuit'

    expect(page).to have_selector(".stream-container .tuit .text", text: text)
  end
end