require 'rails_helper'

RSpec.describe TuitsController, type: :controller do
  
  context 'without user signed in' do
    describe 'POST create' do
      it 'redirect to sign in' do
        post :create, tuit: { user_id: 1, text: 'hola que tal' }
        expect(response).to redirect_to(new_user_session_url)
      end
    end
  end

  context 'with user signed in' do

    let(:user) { build(:user) }

    before(:each) do
      user.skip_confirmation!
      user.save!
      sign_in user
    end

    describe 'POST create' do
      context 'with valid data' do
        let(:tuit_params) { { user_id: user.id, text: 'hola que tal' } }

        it 'redirect to root' do
          post :create, tuit: tuit_params
          expect(response).to redirect_to(root_url)
        end

        it 'create the tuit' do
          post :create, tuit: tuit_params
          tuit = Tuit.find_by(user_id: user.id, text: 'hola que tal')
          expect(tuit).not_to be_nil
        end
      end

      context 'with invalid data' do
        let(:tuit_params) { { user_id: nil, text: 'a'*161 } }

        it 'render timeline' do
          post :create, tuit: tuit_params
          expect(response).to render_template('home/timeline')
        end
      end
    end
  end

end