require 'rails_helper'

RSpec.describe UsersController, type: :controller do


  context 'no user signed in' do
    describe 'GET index' do
      it 'redirect to login' do
        get :index
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    describe 'GET show' do
      it 'redirect to login' do
        get :show, id: 1
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    describe 'GET following' do
      it 'redirect to login' do
        get :following, id: 1
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    describe 'GET followers' do
      it 'redirect to login' do
        get :followers, id: 1
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    describe 'GET tuits' do
      it 'redirect to login' do
        get :tuits, id: 1
        expect(response).to redirect_to(new_user_session_url)
      end
    end
  end

  context 'user signed in' do

    let(:user1) { build(:user) }
    let(:user2) { build(:user, username: 'user2', email: 'user2@test.com') }
    let(:tuit) { build(:tuit, user: user1) }

    before(:each) do
      user1.skip_confirmation!
      user2.skip_confirmation!
      user1.save!
      user2.save!
      user1.followings << user2
      user2.followings << user1
      tuit.save!

      sign_in user1
    end

    describe 'GET index' do
      it 'redirect to login' do
        get :index
        expect(response).to render_template(:index)
      end
    end

    describe 'GET show' do
      before(:each) { get :show, id: user1 }
      
      it 'redirect to login' do
        expect(response).to render_template(:show)
      end

      it 'assigns @following' do
        expect(assigns(:following)).to eq([user2])
      end

      it 'assigns @followers' do
        expect(assigns(:followers)).to eq([user2])
      end

      it 'assigns @following_count' do
        expect(assigns(:following_count)).to eq(1)
      end

      it 'assigns @followers_count' do
        expect(assigns(:followers_count)).to eq(1)
      end

      it 'assigns @tuits' do
        expect(assigns(:tuits)).to eq([tuit])
      end

      it 'assigns @user' do
        expect(assigns(:user)).to eq(user1)
      end

    end

    describe 'GET following' do
      it 'render json' do
        expected_json = user1.followings.to_json
        get :following, id: user1
        expect(response.body).to eq(expected_json)
      end
    end

    describe 'GET followers' do
      it 'render json' do
        expected_json = user1.followers.to_json
        get :followers, id: user1
        expect(response.body).to eq(expected_json)
      end
    end

    describe 'GET tuits' do
      it 'render json' do
        expected_json = user1.tuits.to_json
        get :tuits, id: user1
        expect(response.body).to eq(expected_json)
      end
    end

    describe 'POST/XHR follow' do
      describe 'format js' do
        before(:each){ xhr :post, :follow, id: user2.id }

        it 'render js' do
          expect(response).to render_template(:follow)
        end
        it 'add the relation' do
          relation = Relation.find_by(user_id: user1.id, follow_id: user2.id)
          expect(relation).not_to be_nil
        end
        it 'assigns @relation' do
          relation = Relation.find_by(user_id: user1.id, follow_id: user2.id)
          expect(assigns(:relation)).to eq(relation)
        end

      end
      describe 'format html' do
        let(:referer) { "where_i_came_from" }
        before(:each) do
          request.env["HTTP_REFERER"] = referer
          post :follow, id: user2.id
        end
        
        it 'redirect to referer' do
          expect(response).to redirect_to(referer)
        end
        it 'assigns @relation' do
          relation = Relation.find_by(user_id: user1.id, follow_id: user2.id)
          expect(assigns(:relation)).to eq(relation)
        end
      end
    end

    describe 'POST/XHR unfollow' do
      describe 'format js' do
        it 'render js' do
          xhr :post, :unfollow, id: user2.id
          expect(response).to render_template(:unfollow)
        end
      end
      describe 'format html' do
        it 'render html' do
          referer = "where_i_came_from"
          request.env["HTTP_REFERER"] = referer
          post :unfollow, id: user2.id
          expect(response).to redirect_to(referer)
        end
      end
    end

    describe 'DELETE/XHR unfollow' do
      describe 'format js' do
        before(:each){ xhr :post, :unfollow, id: user2.id }

        it 'render js' do
          expect(response).to render_template(:unfollow)
        end
        it 'remove the relation' do
          relation = Relation.find_by(user_id: user1.id, follow_id: user2.id)
          expect(relation).to be_nil
        end
        it 'assigns @relation' do
          expect(assigns(:relation))
        end

      end
      describe 'format html' do
        let(:referer) { "where_i_came_from" }
        before(:each) do
          request.env["HTTP_REFERER"] = referer
          post :unfollow, id: user2.id
        end
        
        it 'redirect to referer' do
          expect(response).to redirect_to(referer)
        end
        it 'assigns @relation' do
          expect(assigns(:relation))
        end
      end
    end

  end

end