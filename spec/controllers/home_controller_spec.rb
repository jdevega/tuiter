require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  context 'no user signed in' do
    describe 'GET index' do
      it 'redirects to login' do
        get :index
        expect(response).to redirect_to(new_user_session_url)
      end
    end
  end

  context 'user signed in' do
    let(:user) { build(:user) }

    before(:each) do
      user.skip_confirmation!
      user.save!
      sign_in user
      
    end

    it 'render user timeline' do
      get :index
      expect(response).to render_template(:timeline)
    end

    it 'assigns @tuits' do
      tuit = create(:tuit, user: user)
      get :index
      expect(assigns(:tuits)).to eq([tuit])
    end
  end

end