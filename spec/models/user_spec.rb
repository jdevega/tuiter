require 'rails_helper'

RSpec.describe User, type: :model do
  
  describe 'sign_up' do

    it 'is invalid with empty email' do
      user = build(:user, email: nil)
      expect(user).not_to be_valid
    end

    it 'is invalid with empty username' do
      user = build(:user, username: nil)
      expect(user).not_to be_valid
    end

    it 'is invalid with invalid email format' do
      user = build(:user, email: 'hola@quetal')
      expect(user).not_to be_valid
    end

    context 'duplicated data' do

      let(:user1) { build(:user) }
      let(:user2) { build(:user) }

      it 'is invalid if username exists' do
        user1.skip_confirmation!
        user1.save!
        user2.username = user1.username
        expect(user2).not_to be_valid
      end

      it 'is invalid if email exists' do
        user1.skip_confirmation!
        user1.save!
        user2.email = user1.email
        expect(user2).not_to be_valid
      end

      it 'transforms username to lower before validation' do
        user1.skip_confirmation!
        user1.save!
        user2.username = user1.username.upcase
        expect(user2).not_to be_valid
      end

    end

    it 'transforms email into lower' do
      user1 = build(:user, email: 'HOLA@QUE.TAL')
      user1.skip_confirmation!
      user1.save!
      expect(user1.email).to eq('hola@que.tal')
    end

    it 'is invalid with invalid username format' do
      user = build(:user, username: 'hola que ase')
      expect(user).not_to be_valid
      user.username = '$username'
      expect(user).not_to be_valid
      user.username = 'hola.quetal'
      expect(user).not_to be_valid
    end

    context 'password' do
  
      let(:user) { build(:user, password: '', password_confirmation: '') }
      
      it 'is invalid without password and confirmation' do
        expect(user).not_to be_valid
      end

      it 'is invalid without password' do
        user.password = ''
        user.password_confirmation = 'password'
        expect(user).not_to be_valid
      end

      it 'is invalid without password confirmation' do
        user.password = 'password'
        user.password_confirmation = ''
        expect(user).not_to be_valid
      end

      it 'is invalid if password and password_confirmation do not match' do
        user.password = 'password'
        user.password_confirmation = 'differentPassword'
        expect(user).not_to be_valid
      end
    end
  end

  describe 'followings & followers' do

    let(:user1) { build(:user, username: 'user1', email: 'user1@test.com') }
    let(:user2) { build(:user, username: 'user2', email: 'user2@test.com') }

    before(:each) do
      user1.skip_confirmation!
      user1.save!
      user2.skip_confirmation!
      user2.save!
    end

    describe 'followings' do
      it 'can follow other users' do
        user1.followings << user2
        expect(user1.followings).to include(user2)
      end

      it 'can get followers' do
        user1.followings << user2
        expect(user2.followers).to include(user1)
      end

      it 'check user is following another one' do
        user1.followings << user2
        expect(user1.follow?(user2)).to be(true)
      end
    end

    describe 'followers' do
      it 'can be followed' do
        user1.followers << user2
        expect(user1.followers).to include(user2)
      end

      it 'can get followings' do
        user1.followers << user2
        expect(user2.followings).to include(user1)
      end
    end
  end

  context 'tuits' do

    let(:user1) { build(:user, username: 'user1', email: 'user1@test.com') }
    let(:user2) { build(:user, username: 'user2', email: 'user2@test.com') }
    let(:tuit) { build(:tuit) }

    before(:each) do
      user1.skip_confirmation!
      user1.save!
      user2.skip_confirmation!
      user2.save!
      user1.tuits << tuit
    end

    it 'can tuit' do
      expect(user1.tuits).to include(tuit)
    end

    it 'get its own tuits on timeline_tuits' do
      expect(user1.timeline_tuits).to include(tuit)
    end

    it 'get following users tuits on timeline tuits' do
      user1.tuits.delete_all
      user1.followings << user2
      user2.tuits << tuit
      expect(user1.timeline_tuits).to include(tuit)
    end

    it 'do not get other user tuits if not following them' do
      user1.tuits.delete_all
      user2.tuits << tuit
      expect(user1.timeline_tuits).not_to include(tuit)
    end

    context 'sorting' do
    
      it 'sorts an user tuits' do
        tuit2 = build(:tuit)
        user1.tuits << tuit2
        expect(user1.tuits.to_a).to eq([tuit2, tuit])
      end

      it 'sorts an user timeline tuits' do
        tuit2 = build(:tuit)
        user2.tuits << tuit2
        user1.followings << user2
        expect(user1.timeline_tuits.to_a).to eq([tuit2, tuit])
      end

    end

  end

end
