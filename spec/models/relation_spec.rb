require 'rails_helper'

RSpec.describe Relation, :type => :model do
  
  it 'fails to create a duplicated following' do
    user1 = build(:user)
    user2 = build(:user)
    user1.skip_confirmation!
    user2.skip_confirmation!
    user1.save!
    user2.save!

    create(:relation, user_id: user1.id, follow_id: user2.id)
    duplicated = build(:relation, user_id: user1.id, follow_id: user2.id)
    expect(duplicated).not_to be_valid
  end

end
