require 'rails_helper'

RSpec.describe Tuit, type: :model do
  
  describe 'validations' do

    let(:user) { build(:user) }
    let(:tuit) { build(:tuit) }

    before(:each) do
      user.skip_confirmation!
      user.save!
    end

    it 'is invalid without associated user' do
      tuit.user_id = nil
      expect(tuit).not_to be_valid
    end

    it 'is invalid with empty text' do
      tuit.text = ''
      expect(tuit).not_to be_valid
    end

    it 'is invalid with long text' do
      tuit.text = 'a'*161
      expect(tuit).not_to be_valid
    end

  end

end
