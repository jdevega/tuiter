# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'faker'

def rand_time(from, to=Time.now)
  Time.at(rand_in_range(from.to_f, to.to_f))
end

def rand_in_range(from, to)
  rand * (to - from) + from
end


20.times do |i|
  u = User.create(
    username: "username_#{i+1}", 
    name: "User #{i+1}", 
    email: "joseluis.devega+#{i+1}@gmail.com", 
    password: 'redradix', 
    password_confirmation: 'redradix',
    confirmed_at: Time.now)
end

users = User.all
users.each do |user|
  2.times do 
    user2 = users.all_except(user).sample
    user.followings << user2 if user.followings.exclude? user2
  end
  3.times do
    user2 = users.all_except(user).sample
    user.followers << user2 if user.followers.exclude? user2
  end
end

100.times do 
  created_at = rand_time(10.days.ago)
  Tuit.create(user_id: users.sample.id, text: Faker::Lorem.sentence(11), created_at: created_at)
end
