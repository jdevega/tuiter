class CreateRelations < ActiveRecord::Migration
  def change
    create_table :relations do |t|
      t.references :user, index: true, foreign_key: true
      t.integer :follow_id, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
